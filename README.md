This repository has the script that will merge OPS PDF files into a single large PDF file. In the merged file the OPSs will be ordered by subgroup and within subgroups they will be ordered according to the group matrix. 

# Inputs and Running

## Dependancies 
This is a Python 3 script. So you should have python 3 installed an in your PATH. It uses two non-standard dependancies `PyPDF2` and `fpdf`. If you have `pip` working you should be able to install these with `pip install PyPDF2` and `pip install fpdf` 

## Required inputs
In order to run the script you need to provide a few inputs. 

First you need the group matrix saved as a _.csv_ file. An example is provided in the repository, saved as `GroupMatrix.csv`. The easiest thing to do would be to edit the example according to the current group memebers. 

Next you need a directory which contains all of the OPSs you want to merge. Let's call that directory `DIR`. The OPSs can be organized in sub-directories in any way. So for example you might have a directory called `July 2020` which contains all the OPSs (saved as PDFs, format constraints below). Or you could have `July 2020` which could contain `Inorganic Biology`, `Hybrids`, `Digital Chemistry` and `Molecular` sub-folders and saved OPSs within those sub-folders. The point is that as long as the OPSs are somewher in the directory, the strucutre within that directory can be organized however you want. 

Finally the OPSs need to be saved as PDFs with the Last name (surname etc) of the individuals in the name of the file. The file name could contain other information but it must contain at least the last name of the individual. For example `OPS_Mathis_30072020.pdf`, `Mathis_OPS.pdf`, `Mathis.pdf` will all work so long as they are saved in the directory `DIR` mentioned above. 

## Running 
To run the script you need to edit the file `merger.py`. At the bottom of the file you can specify the inputs (line ~200). Set the variable `DIRNAME` equal to a string with the name of the directory mentioned above. Set the variable `GROUPMATRIX` equal to the filename of the group matrix _.csv_ file from above. Save these changes to `merger.py` 

Run `python merger.py` in this directory. (You will probably see a bunch of random whitespace warnings)

If the script runs without error a file named `Merged_OPS.pdf` will be created. Check that is it correct. 


# Troubleshooting tips 

If the script gave you an error or a Merged OPS that isn't right here's a few things to think about:

All the information in the script comes from the Group Matrix.csv and the file names in the specified directory. If the names in the CSV and the files don't match up that could cause some problems. 

## The script ran but the merged OPS doesn't have all the files 
If you're missing one or a few OPSs check the spelling on the files and the group matrix csv, do the last name spellings match? This might be the cause of a lot of minor errors.


## The script didn't finish running, it's complaining about "UnicodeEncodeError: 'latin-1' codec can't encode characters in position 8-9: ordinal not in range(256)"
This issue came up with one OPS in May 2020. It's a weird encoding error but there's hope to solve it. I solved this by installing `Ghostscript` and using [this stack exchange answer](https://superuser.com/questions/278562/how-can-i-fix-repair-a-corrupted-pdf-file)

Basically you need to identify the offended PDF and fix the corrupted file using the instructions above. Then delete the old file and replace it with the new (uncorrupted) one you just made. Run the sript again and the problem should be sorted. This might be something you need Graham or Matthew you help with.

The hard part should be identifying the offending/corrupted PDF. I haven't figured out an efficient way to do this yet. The place to start would be by merging one subgroup at a time and then working down to individual PDFs from there. 

The first instance of this problem came from Shan She's May 2020 pdf. If it's a software problem its worth starting with previous offenders 

