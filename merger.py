import PyPDF2
from fpdf import FPDF
import csv
import os
from pathlib import Path

def merge_in_order(pdf_fnames, subgroup_orders, people_orders):
    """ This function will merge the pdfs, saved in pdf_fnames.
        The arguments to the function are 
        - pdf_fnames: a list of pdf file names, these MUST contain at least 
            the last name of the author,
        - subgroup_orders, is a list of tuples where the first element in each
            tuple is the subgroup name, and the second element is the position
            of the subgroup in the list
        - people_orders is a list of tuples, the first position in the tuple is
            the person's last name, the second position is the subgroup of the 
            person, and the third position is their order in the subgroup
        
        returns a merged pdf object to be saved
    """
    ### Check dimensions

    ## Check all subgroups are accounted fors

    ## Check get people to pdf dict
    people_pdf_dict, problem_information = get_people_pdf_dict(pdf_fnames, people_orders)

    print(problem_information.keys())
    subgroup_orders = sorted(subgroup_orders, key=lambda x: x[1])
    ordered_pdf_fnames = []
    for subgroup in subgroup_orders:
        print(subgroup)
        # Get this subgroup
        this_sub = subgroup[0]
        # Get this subgroups people and sort
        this_subs_people = [person for person in people_orders if person[1]== this_sub]
        this_subs_people = sorted(this_subs_people, key=lambda x: x[2]) 
        # Get the problem information for this group
        this_subs_problems = problem_information[this_sub]
        # Write the page for subgroup problems and get the path
        subgroup_page_path = make_subgroup_page(
            this_subs_problems, this_sub, directory="")
        # Put the subgroup problem page upfront 
        ordered_pdf_fnames.append(subgroup_page_path)
        # Add all the paths for the subgroup 
        # Append each person's pdf in order
        for person in this_subs_people:
            personal_pdf =  people_pdf_dict.get(person[0], False)
            if personal_pdf != False:
                ordered_pdf_fnames.append(personal_pdf)
        # Put the orphaned pdfs at the end 
        orphaned_files = this_subs_problems["Files"]
        ordered_pdf_fnames.extend(orphaned_files)

    merged_fname = merger("Merged_OPS.pdf", ordered_pdf_fnames)     
    return merged_fname

def get_subgroups_and_names(group_matrix_fname):
    """ This function will read the group matrix as a csv and return the 
        names and positions of its members as a list of tuples to be 
        fed into merge_in_order(...)
        Arguments:
        -group_matrix_fname: the file name of a group matrix saved as a
            csv. See README for more details/constraints
    """
    
    # Read in the data as an array
    with open(group_matrix_fname) as csvfile:
        data = list(csv.reader(csvfile))

    # Initalize some variables
    current_subgroup = ""
    subgroup_position = 1 
    # We'll return the list below
    people_orders = []
    for l in data:
        # For each line fo the csv
        if "subgroup" in l[0].lower(): # if you've got subgroup in the line, its a new subgroup
            current_subgroup = l[0] # Get the new subgroup
            subgroup_position = 1  # Update order int 
        elif l[1] !='': # If the name isn't blank
            names = l[1]    # Get the names 
            names = names.replace("(HM group)", "") # Delete HM Group info
            names = names.strip() # Strip whitespace 
            split_names = names.split(' ')
            last_name = split_names[-1]  # Get the last name
            last_name= last_name.strip() # Strip whitespace again 
            # Append in order 
            people_orders.append( (last_name, current_subgroup, subgroup_position) )
            subgroup_position += 1

    return people_orders

def get_all_pdfs(directory_to_check):
    """ This function will recursively identify files in the 
        directory to check that end in .pdf
    """
    pdf_fnames = Path(directory_to_check).rglob('*.pdf')
    keep_names = [str(pdf) for pdf in pdf_fnames if pdf.name[0] != '.']
    return keep_names


def get_people_pdf_dict(pdf_fnames, people_orders):
	""" Make a dictionary that maps peoples last names to their pdf files
	Keep track of people who don't have pdfs and pdfs who don't have people
	Arguments:
	- pdf_fnames: List of Pdf files in the directory
	- People Orders: list of tuples (last name, subgroup, order)
	"""
	people_pdf_dict = {}
	missing_people = [] # People without pdfs
	orphaned_pdfs = [] # Pdfs without people

	list_of_people = [p[0] for p in people_orders] # People orders is a list of tuples

	# For each person
	for person in list_of_people:
		for pdf in pdf_fnames: # Check if their name is in the pdf 
			lowercase_pdf = pdf.lower()
			lowercase_person = person.lower()
			if lowercase_person in lowercase_pdf:
				people_pdf_dict[person] = pdf 
	# List of pdfs that were never assigned           
	orphaned_pdfs = [pdf for pdf in pdf_fnames if pdf not in people_pdf_dict.values()]
	print(orphaned_pdfs)
	# List of people without pdf 
	missing_people = [person for person in people_orders if person[0] not in people_pdf_dict.keys()]
	# Get the information for each subgroup and keep track of the problems 
	all_subgroups = set([p[1] for p in people_orders])
	problem_information = {}
	# For each subgroup
	for group in all_subgroups:
		print(group)
		group_motif = group#[0:4].lower()
		# Get their files 
		this_groups_files = [pdf for pdf in orphaned_pdfs if group_motif in pdf]
		# Their people 
		this_groups_people = [person[0] for person in missing_people if person[1] == group]
		# THeir problems 
		this_subgroups_problems = {"People": this_groups_people, "Files": this_groups_files}
		problem_information[group] = this_subgroups_problems

	return people_pdf_dict, problem_information


def make_subgroup_page(problem_information, subgroup, directory =''):
    
    ### Boiler Plate stuff
    pdf = FPDF() # Initialize the pdf
    pdf.add_page() # Make a blank page
    output_path = directory + subgroup + ".pdf" # Build the output filename
    # Set the header
    pdf.set_xy(0, 20) # Location
    pdf.set_font('arial', 'BU', 18.0) # Font
    pdf.cell(ln=0, h=5.0, align='C', w=0, txt=subgroup, border=0) # Text

    # List the missing people 
    pdf.set_xy(10, 50) # Location
    pdf.set_font('arial', 'BU', 14.0) # Font
    pdf.cell(ln=1, h=5.0, align='L', w=0, txt="Missing OPS", border=0) # Text
    pdf.set_font('arial', '', 14.0) # Make the font not bold or underlined

    missing_people = problem_information["People"]
    for p in missing_people: # Add all the missing people 
        pdf.cell(ln=1, h=5.0, align='L', w=0, txt=p, border=0) # ln =1 tells the writer
        # to skip to the next line after writing 

    # List the orphaned PDFs
    pdf.set_font('arial', 'BU', 14.0) # Same as above
    # Add some new lines
    pdf.cell(ln=1, h=5.0, align='L', w=0, txt="", border=0)
    pdf.cell(ln=1, h=5.0, align='L', w=0, txt="", border=0)
    pdf.cell(ln=1, h=5.0, align='L', w=0, txt="Unmatched PDF", border=0)
    orphaned_pdfs = problem_information["Files"]
    for f in orphaned_pdfs:
        base_name = os.path.basename(f)
        pdf.cell(ln=1, h=5.0, align='L', w=0, txt=base_name, border=0)
    
    # Save the file
    pdf.output(output_path, 'F')
   
    return output_path


def merger(output_path, input_paths):
		# Init merger object
		pdf_merger = PyPDF2.PdfFileMerger()
		# For each file 
		for path in input_paths:
			# Merge only the first page
			print(path)
			pdf_merger.append(path, pages = (0,1))
		# Write to file 
		with open(output_path, 'wb') as fileobj:
			#encoded_obj = fileobj.encode("utf-8")
			pdf_merger.write(fileobj)
		# Return saved path
		return output_path


### DEFINE INPUTS HERE ######
DIRNAME = "Oct 2020"
GROUPMATRIX = "GroupMatrix.csv"
#################################

# Get all the pdfs 
pdf_fnames = get_all_pdfs(DIRNAME)
# Get the names, and orders for all the subgroups 
people_orders = get_subgroups_and_names(GROUPMATRIX)

# This is the order of the subgroups 
subgroup_orders = [("Wall-E Subgroup", 4),
                   ("Molecules Subgroup", 2), 
                   ("Gaia Subgroup", 1), 
                   ("Aliens Subgroup", 3)]
print(subgroup_orders)
merge_in_order(pdf_fnames, subgroup_orders, people_orders)
